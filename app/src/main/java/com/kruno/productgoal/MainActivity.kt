package com.kruno.productgoal

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvApiKey.text = BuildConfig.API_KEY
        tvRequestorToken.text = BuildConfig.REQUESTOR_TOKEN
        tvZendeskAppId.text = BuildConfig.ZENDESK_APP_ID
        tvLeanPlumAppId.text = BuildConfig.LEANPLUM_APP_ID
        tvGooglePlayLicenceKey.text = BuildConfig.APP_GOOGLE_PLAY_LICENCE_KEY
    }
}
